// For a given input stream create a file if none exists where 
// label is non-zero.
// if label is zero create file dump for them

#ifndef STRIP_H_
#define STRIP_H_

#include "file_util.h"

#include "rxcpp/rx.hpp"
namespace Rx {
using namespace rxcpp;
using namespace rxcpp::sources;
using namespace rxcpp::operators;
using namespace rxcpp::util;
}
using namespace Rx;

#include <string>

namespace muscle {
    namespace shred {
        class Strip {
            Strip();
            
            bool generateFile(const std::string &filename);
            bool fileExists(const std::string &filename);
            void writeLines(observable<std::string> lines);

            filey::util::FileUtil fUtil_;
            
        };
    } // shred
} // muscle

#endif // STRIP_H_