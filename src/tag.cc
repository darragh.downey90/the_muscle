#include "tag.h"

muscle::snap::Tag::Tag()
{

}

std::unique_ptr<zmq::socket_t> muscle::snap::Tag::worker_socket(zmq::context_t &ctx)
{
    std::unique_ptr<zmq::socket_t> worker = std::make_unique<zmq::socket_t>(ctx, ZMQ_DEALER);

    // set random identity to make tracing easier
    this->identifier_ = "";
    worker->connect("tcp://localhost:5557");

    // configure socket to not wait at close time
    int linger = 0;
    worker->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));

    // inform queue that worker is ready for work
    std::cout << "I: (" << this->identifier_ << ") worker ready" << std::endl;
    worker->send("READY", 0);

    return worker;
}

std::string muscle::snap::Tag::getId()
{
    return this->identifier_;
}