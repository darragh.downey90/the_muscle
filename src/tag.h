// Listen for connections and messages from 
// the juggler.
// Respond with success on completion

#ifndef TAG_H_
#define TAG_H_

#include "zmq.hpp"
#include "zmq_addon.hpp"

#include <iostream>

namespace muscle {
    namespace snap {

        class Tag {
            public:
            Tag();
            std::unique_ptr<zmq::socket_t> worker_socket(zmq::context_t &ctx);

            std::string getId();

            private: 
            std::string identifier_;

        };
    } // snap
} // muscle

#endif // TAG_H_