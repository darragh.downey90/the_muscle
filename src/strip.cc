#include "strip.h"

muscle::shred::Strip::Strip()
{
    this->fUtil_ = new filey::util::FileUtil();
}

bool muscle::shred::Strip::generateFile(const std::string &filename)
{
    return this->fUtil_.createFile(filename);
}

bool muscle::shred::Strip::fileExists(const std::string &filename)
{
    if(this->fUtil_.fileExists(filename))
    {
        return true;
    }
    
    return generateFile(filename);
}

void muscle::shred::Strip::writeLines(observable<std::string> lines) 
{
    // don't know if I need this but see here for an example https://github.com/ReactiveX/RxCpp/blob/master/Rx/v2/examples/doxygen/subscribe.cpp
    // auto subscription = composite_subscription();

    lines.subscribe(
        [&](std::string v) 
        {
            if (!v.empty()) 
            {
                if(!fileExists(v))
                {
                    generateFile(v);
                    // open file and append
                }
                else
                {
                    // open file and append
                }
                std::printf("OnNext: %s\n", v.c_str()); 
            }
        },
        [&](std::exception_ptr ep)
        {
            try { std::rethrow_exception(ep);}
            catch (const std::exception &e) 
            {
                std::printf("OnError: %s\n", e.what());
            }
        },
        []()
        { 
            std::printf("OnCompleted!\n");
        }
    );
}