#include "tag.h"
#include "strip.h" 
// may not be included here but elsewhere, as 
// zmq would pass the message

#include "zmq.hpp"
#include "zmq_addon.hpp"
#include <zmq.h>

#include <time.h>

namespace muscle {

    #define HEARTBEAT_LIVENESS 3       // 3-5 is reasonable
    #define HEARTBEAT_INTERVAL 1000    // msecs
    #define INTERVAL_INIT      1000    // initial reconnect
    #define INTERVAL_MAX       32000   // after exponential backoff


    // see https://github.com/zeromq/cppzmq/blob/f81accd6b11ab76ad39fee966bd01c64cbcce4d9/tests/multipart.cpp
    // for zmq::multipart_t api
    int main()
    {
        std::unique_ptr<snap::Tag> t = std::make_unique<snap::Tag>();
        zmq::context_t context(1);
        std::unique_ptr<zmq::socket_t> worker = t->worker_socket(context);

        // If liveness hits zero, queue is considered disconnected
        size_t liveness = HEARTBEAT_LIVENESS;
        size_t interval = INTERVAL_INIT;

        // Send heartbeats out at regular intervals
        int64_t heartbeat_at = clock() + HEARTBEAT_INTERVAL;

        int cycles = 0;

        // continuous loop
        while(1) 
        {
            zmq::pollitem_t items [] = { { *worker, 0, ZMQ_POLLIN, 0} };
            zmq::poll (items, 1, HEARTBEAT_INTERVAL);

            zmq::multipart_t msg;

            if (items[0].revents & ZMQ_POLLIN)
            {
                // Get message
                // - 3-part envelope + content -> request
                // - 1-part "HEARTBEAT" -> heartbeat

                // received a work order
                if (msg.size() == 3) 
                {
                    // simulate various problems, after a few cycles
                    // won't need this but useful for illustration purposes
                    ++cycles;

                    // if (cycles > 3 && zmq::within(5) == 0) 
                    // {
                    //    std::cout << "I: (" << identifier_ << ") simulating a crash\n";
                    //    msg.clear();
                    //    break;
                    // } 
                    // else 
                    // {
                    //    if (cycles > 3 && zmq::within(5) == 0) 
                    //    {
                    //        std::cout << "I: (" << identifier_ << ") simulating CPU overload\n";
                    //        sleep(5);
                    //    }
                    // }

                    // actual work to be done here
                    std::cout << "I: (" << t->getId() << ") normal reply - " << msg.at(1) << "\n";
                    msg.pushstr("MSG RECEIVED");
                    msg.send(*worker, 0);
                    liveness = HEARTBEAT_LIVENESS;
                    // sleep(1); // do some heavy work, ie. process

                } 
                else 
                {
                    if (msg.size() == 1 && msg.at(1).str() == "HEARTBEAT") 
                    {
                        liveness = HEARTBEAT_LIVENESS;
                    } 
                    else 
                    {
                        std::cout << "E: (" << t->getId() << ") invalid message" << std::endl;
                        msg.clear();
                    }
                }

                interval = INTERVAL_INIT;

            } 
            else if (--liveness == 0) 
            {
                std::cout << "W: (" << t->getId() << ") heartbeat failure, can't reach queue\n";
                std::cout << "W: (" << t->getId() << ") reconnecting in " << interval << " msec...\n";
                // sleep(interval);

                if (interval < INTERVAL_MAX) {
                    interval *= 2;
                }

                worker.reset();
                worker = t->worker_socket(context);
                liveness = HEARTBEAT_LIVENESS;
            }

            // send heartbeat to queue if it's time
            if (clock() > heartbeat_at) 
            {
                heartbeat_at = clock() + HEARTBEAT_INTERVAL;
                std::cout << "I: (" << t->getId() << ") worker heartbeat\n";
                msg.pushstr("HEARTBEAT");
                msg.send(*worker, 0);
            }
        }

        worker.reset();

        return 0;
    }

}

