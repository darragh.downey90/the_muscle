#include "gtest/gtest.h"

#include "src/strip.h"

#include <vector>
#include <string>

namespace 
{
    class StripTest : public ::testing:Test {
        protected:
          // you can remove any or all of the following functions if its body is empty
          StripTest() {
              // You can do set-up work for each test here
          }

          virtual ~StripTest() {
              // you can do clean-up work that doesn't throw exceptions here
          }

          // If the constructor and destructor are not enough for setting up
          // and cleaning up each test, you can define the following methods:

          virtual void SetUp() {
              // Code here will be immediately called after the constructor
              // (right before each test)
              std::vector<std::string> lines_of_data = {
                "1483189211.07142;6bd8140f-2cf4-3cd1-b2a9-34840be160a9;",
                "1483189211.07166;8ceed5c7-d6d2-3028-33d0-c021cf269dee;D3 2121 SK",
                "1483189211.08491;326e37e4-c754-3831-4b9e-4b75b8701937;",
                "1483189211.08515;7aabd287-95c8-32be-d0b0-ef3c5c7546f6;D3 2122 TH",
                "1483189212.14028;7dca1741-184f-3a9e-e7c1-74d59d183b10;",
                "1483189212.14053;f47b09fe-c253-3b99-b447-b756bfc7d7d2;D3 2118 BM",
                "1483189223.08491;571598c5-34a3-369f-c68d-0e2b8eee64cb;",
                "1483189223.08517;44567c94-202f-37e2-3b39-b9f42455f48e;D3 2115 JK",
                "1483189232.09028;610e2e4e-680d-3ce5-1dfb-78805320b6ac;",
                "1483189232.09049;83c6e1f1-671c-3f3c-cecd-5012c76722aa;D3 2114 DT",
                "1483189240.34114;8ceed5c7-d6d2-3028-33d0-c021cf269dee;",
                "1483189240.34137;304be064-d869-332d-a882-e5d6be8270ef;D3 2121 SK",
                "1483189251.94694;03f68cc8-febd-38a8-ffa2-b542d98a186c;",
                "1483189251.94708;fa9cd39c-f7ff-31aa-c312-3d116f4410c3;D3 2112 RV",
                "1483189256.82322;304be064-d869-332d-a882-e5d6be8270ef;",
                "1483189256.82352;1a8f11bb-6dbd-3312-1e26-a02c9b84cbf4;D3 2121 SK",
                "1483189258.49363;f933b03e-c6bc-3857-3130-62fd897b8e5c;",
                "1483189258.4939;fdc6d208-b82d-3a31-aa61-ecf363760d29;D3 2119 FH",
                "1483189272.12056;7aabd287-95c8-32be-d0b0-ef3c5c7546f6;",
                "1483189272.12078;6f358dab-7b63-3376-922d-8d40086b53ed;D3 2122 TH",
                "1483189282.90131;f47b09fe-c253-3b99-b447-b756bfc7d7d2;",
                "1483189282.90152;f24b3e63-f0c5-3979-ec7d-e8663e4fcddc;D3 2118 BM",
                "1483189285.52705;856cdafe-4fe2-3191-5b71-bf90be74b49d;",
                "1483189285.52736;764253ac-482e-387a-fd88-086b956039bb;D3 2120 AJ",
              };

              auto lines = from(lines_of_data); 
          }

          virtual void TearDown() {
              // Code here will be called immediately after each test 
              // (right before the destructor)
          }

          // Objects declared here can be used by all tests in the test case for ProcessScada
    };

    TEST_F(Strip, GenerateFiles) { 
        // for an arraylist of filenames
        // create files of those names in the given directory
        std::vector<std::string> files_to_create = {"2121", "2122", "2118", "2115",
        "2114", "2112", "2119", "2120"};

        for(auto f: files_to_create) 
        {
            EXPECT_EQ(true, muscle::shred::Strip::generateFile(f));
        }
    }

    TEST_F(Strip, CheckFiles) {
        // given a directory of files
        // check that fileExists returns true for file exists
        // and false for a non-existent file

        EXPECT_EQ(true, muscle::shred::Strip::fileExists("2115"));
        EXPECT_EQ(true, muscle::shred::Strip::fileExists("2112"));
        EXPECT_EQ(true, muscle::shred::Strip::fileExists("2116"));
        EXPECT_EQ(false, muscle::shred::Strip::fileExists("2113"));
        EXPECT_EQ(true, muscle::shred::Strip::fileExists("2118"));
        EXPECT_EQ(false, muscle::shred::Strip::fileExists("2125"))
    }

    TEST_F(Strip, WriteToFile) {
        // test we can open the file
        // test we have write permissions
        // write to file
        // close and then reopen file
        // read file contents and compare with what we supposedly wrote

        muscle::shred::Strip::writeToFile(lines);

    }
} // 




