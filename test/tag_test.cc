#include "gtest/gtest.h"

#include "rxcpp/rx.hpp"
namespace Rx {
using namespace rxcpp;
using namespace rxcpp::sources;
using namespace rxcpp::operators;
using namespace rxcpp::util;
}
using namespace Rx;

#include "src/tag.h"

#include <vector>
#include <string>

namespace 
{
    class TagTest : public ::testing:Test {
        protected:
          // you can remove any or all of the following functions if its body is empty
          TagTest() {
              // You can do set-up work for each test here
          }

          virtual ~TagTest() {
              // you can do clean-up work that doesn't throw exceptions here
          }

          // If the constructor and destructor are not enough for setting up
          // and cleaning up each test, you can define the following methods:

          virtual void SetUp() {
              // Code here will be immediately called after the constructor
              // (right before each test)
          }

          virtual void TearDown() {
              // Code here will be called immediately after each test 
              // (right before the destructor)
          }

          // Objects declared here can be used by all tests in the test case for ProcessScada
    };
} // 




