#
# MIT License
#
# Copyright (c) 2018 Darragh Downey
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


###############################################################################################################################

http_archive(
    name = "filey",
    urls = ["https://gitlab.com/darragh.downey90/filey/-/archive/master/filey-master.zip"],
    strip_prefix = "filey-master",
)

###############################################################################################################################

new_http_archive(
    name = "gtest",
    url = "https://github.com/google/googletest/archive/release-1.8.0.zip",
    build_file = "third_party/gtest/gtest.BUILD",
    strip_prefix = "googletest-release-1.8.0/",
)

###############################################################################################################################

new_http_archive(
    name="rxcpp",
    url="https://github.com/Reactive-Extensions/RxCpp/archive/v4.1.0.zip",
    build_file="third_party/rxcpp/rxcpp.BUILD",
    strip_prefix = "RxCpp-4.1.0"
)

###############################################################################################################################
# See https://stackoverflow.com/questions/43220067/bazel-link-so-libraries-located-in-a-completely-different-very-remote-folder
# libzmq static libraries
new_local_repository(
    name="libzmq_libs",
    path="/usr/local/lib",
    build_file_content="""
cc_library(
    name="libzmq",
    srcs=["libzmq.a"],
    visibility=["//visibility:public"],
    copts=["-Iexternal/libzmq_libs"]
)
    """,
)

# need to download and build OR use apt

###############################################################################################################################

# libzmq header files
new_local_repository(
    name="libzmq_hdrs",
    # pkg-config --variable=libdir libzmq
    path="/usr/local/include",
    build_file_content="""
cc_library(
    name="zmq",
    hdrs=["zmq.h"],
    visibility=["//visibility:public"],
    copts=["-Iexternal/libzmq_hdrs"]
)
    """,
)

# need to download and build OR use apt

###############################################################################################################################

new_http_archive(
    name="zmq",
    url="https://github.com/zeromq/cppzmq/archive/master.zip",
    build_file="third_party/zmq/cppzmq.BUILD",
    strip_prefix="cppzmq-master"
)

###############################################################################################################################

http_archive(
    name="flatbuffers",
    urls=["https://github.com/google/flatbuffers/archive/v1.9.0.zip"],
    strip_prefix="flatbuffers-1.9.0"
)

###############################################################################################################################

