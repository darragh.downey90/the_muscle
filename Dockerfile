# Docker multi-stage build
# See https://imantabrizian.me/posts/2017/06/docker-multistage for C, C++, Golang example
#
# Stage 0: Build stage
#
# Install all requirements for compiling Trippy and run all tests
#
FROM ubuntu:bionic as build-stage

# Expect path to SSH key
# ARG SSH_PRV_KEY
# ARG SSH_PUB_KEY

# Authorize SSH host
# RUN mkdir -p /root/.ssh/
# RUN chmod 0700 /root/.ssh

# Install tools required to build the project
# We need to run `docker build --no-cache .` to update those dependencies
RUN apt update && apt install -y software-properties-common apt-utils xz-utils build-essential wget gnupg2 gpg-agent ssh --no-install-recommends

# Let's see if g++ can do a better job

# RUN rm -rf /var/lib/apt/lists/* && wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - 

# RUN add-apt-repository "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-6.0 main" 

# RUN apt-get update && apt-get install -y clang-6.0 lldb-6.0 lld-6.0 --no-install-recommends 

# RUN ln -s /usr/bin/clang-6.0 /usr/bin/clang && ln -s /usr/bin/clang++-6.0 /usr/bin/clang++ 

# Install bazel 
RUN apt update && apt install -y g++ zlib1g-dev unzip pkg-config zip python git libtool autoconf automake libkrb5-dev --no-install-recommends

RUN wget https://github.com/bazelbuild/bazel/releases/download/0.15.0/bazel-0.15.0-installer-linux-x86_64.sh && chmod +x bazel-0.15.0-installer-linux-x86_64.sh \
    && ./bazel-0.15.0-installer-linux-x86_64.sh && export PATH="$PATH:$HOME/bin"

# I would very much like bazel to handle this but until I crack the problem
# this will have to do...
# Reason for wanting Bazel - not everyone will want to use Docker, so Bazel needs
# to download and compile as many of the required external dependencies as possible

# The line below is pulled from https://github.com/zeromq/libzmq
RUN cd /tmp && git clone git://github.com/jedisct1/libsodium.git && cd libsodium && git checkout e2a30a && ./autogen.sh && ./configure && make check && make install && ldconfig

RUN cd /tmp && git clone --depth 1 git://github.com/zeromq/libzmq.git && cd libzmq && ./autogen.sh && ./configure && make

# Need to figure out how to copy the built 0MQ to the production image    
RUN cd /tmp/libzmq && make install && ldconfig

# reduce size of package by deleting tmp subfolders
RUN rm /tmp/* -rf

# Add Gitlab to list of known hosts
# RUN ssh-keyscan gitlab.com > /root/.ssh/known_hosts

# Copy the SSH key across to the given location
# COPY ${SSH_KEY_FILE} /ssh/id_rsa
#RUN echo "$SSH_PRV_KEY" > /root/.ssh/id_rsa \
#    && echo "$SSH_PUB_KEY" > /root/.ssh/id_rsa.pub \
#    && chmod 600 /root/.ssh/id_rsa \
#    && chmod 600 /root/.ssh/id_rsa.pub

# Check if ssh agent is running or not, if not, run
#RUN eval `ssh-agent -s` && ssh-add /root/.ssh/id_rsa

# Copy all project and build it
# This layer is rebuilt whenever a file has changed in the project directory
COPY . /the_muscle/
WORKDIR /the_muscle

# Build the project!
# Compile 
# NOTE: Flatbuffer schema will have to be compiled in Bazel at runtime. See 
# https://github.com/tensorflow/tensorflow/blob/b390be62ad0514f4fd3347b9db3446c84e08a38e/tensorflow/contrib/lite/schema/BUILD
# https://github.com/tensorflow/tensorflow/tree/b390be62ad0514f4fd3347b9db3446c84e08a38e/third_party/flatbuffers 
# for how to configure and compile flatbuffer schema files.

# RUN bazel build --cxxopt='-std=c++17' //src:trippy
RUN CC=/usr/bin/g++ bazel build --cxxopt="-std=c++17" --cxxopt="-lc++experimental" //:the_muscle --verbose_failures --sandbox_debug

# Remove the secret just in case its dir gets copied below
# RUN rm -vf /root/.ssh/*

#
# Stage 1: Production
#
# Run the Trippy service
#
FROM alpine:latest

WORKDIR /app 

COPY --from=build-stage /app/dist/out/ .

CMD ["./app/app_binary.out"]
